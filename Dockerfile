######################## Base Args ########################
ARG BASE_REGISTRY=registry1.dso.mil
ARG BASE_IMAGE=ironbank/redhat/ubi/ubi8
ARG BASE_TAG=8.5

ARG UBI_BASE_IMAGE=${BASE_REGISTRY}/${BASE_IMAGE}:${BASE_TAG}

FROM ${UBI_BASE_IMAGE} as builder

USER 0

COPY ruby-27.tar.gz gdbm.tar.gz scripts/*.sh /

ARG PACKAGES="gcc-c++ patch readline zlib zlib-devel libffi-devel libedit \
    openssl-devel make bzip2 autoconf automake libtool sqlite-devel libpq-devel"

RUN dnf install -y --setopt=tsflags=nodocs $PACKAGES \
    && chmod +x /install-ruby.sh /install-gdbm.sh \
    && /install-gdbm.sh \
    && /install-ruby.sh \
    && rm -f /install-ruby.sh /install-gdbm.sh \
    && rm -rf /var/cache/dnf/ /var/tmp/* /tmp/* /var/tmp/.???* /tmp/.???*

FROM ${UBI_BASE_IMAGE}

RUN dnf update -y \
    && dnf clean all \
    && rm -rf /var/cache/dnf/ /var/tmp/* /tmp/* /var/tmp/.???* /tmp/.???*

COPY --from=builder /usr/local/bin/ /usr/local/bin/
COPY --from=builder /usr/local/lib/ /usr/local/lib/
COPY --from=builder /usr/local/include/ /usr/local/include/
COPY rdoc.gem /

ENV RUBY_MAJOR=2 \
    RUBY_MINOR=7

ENV HOME=/opt/app-root/src
ENV GEM_HOME /usr/local/bundle
ENV BUNDLE_SILENCE_ROOT_WARNING=1
ENV PATH $HOME/bin:$GEM_HOME/bin:$PATH

RUN mkdir -p "$GEM_HOME" && \
    chmod 755 "$GEM_HOME" && \
    chown -R 1001:0 "$GEM_HOME"

RUN useradd -u 1001 -g 0 -M -d /opt/app-root/src default && \
    mkdir -p /opt/app-root/src && \
    chown -R 1001:0 /opt/app-root && \
    gem install /rdoc.gem && \
    rm -f /rdoc.gem

USER 1001

HEALTHCHECK NONE

CMD ["irb"]